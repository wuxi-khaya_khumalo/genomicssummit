<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod('understrap_container_type');
?>

<?php get_template_part('sidebar-templates/sidebar', 'footerfull'); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="container" id="sponsors2020">
		<h1 class="main-title-2020"><?= the_field("section_sponsors_title") ?></h1>
	</div>

	<div class="<?php echo esc_attr($container); ?>">

		<div class="row">

			<?php if (have_rows('sponsors')) :
				while (have_rows('sponsors')) : the_row();
			?>
					<?php if (have_rows('gold_sponsors')) :
						while (have_rows('gold_sponsors')) : the_row();
					?>
							<div class="col-sm-6 col-md-3 col-xs-12 col-md-offset-1">
								<div class="sponsor-wrap">
									<a target="_blank" href="<?= get_sub_field('sponsor_link'); ?>">
										<img src="<?= get_sub_field('sponsor_image'); ?>" class="center-block img img-responsive" />
									</a>
								</div>
							</div>
					<?php endwhile;
					else :
					endif; ?>

					<?php if (have_rows('silver_sponsors')) :
						while (have_rows('silver_sponsors')) : the_row();
					?>
							<div class="col-sm-6 col-md-2 col-xs-12">
								<div class="sponsor-wrap">
									<a target="_blank" href="<?= get_sub_field('sponsor_link'); ?>">
										<img src="<?= get_sub_field('sponsor_image'); ?>" class="center-block img img-responsive" />
									</a>
								</div>
							</div>
					<?php endwhile;
					else :
					endif; ?>

					<?php $maxSizeBronzes = count(get_sub_field('bronze_sponsors')); ?>
					<?php $count = 0; ?>

					<?php if (have_rows('bronze_sponsors')) :
						while (have_rows('bronze_sponsors')) : the_row();
					?>
							<?php if ($count == 0) : ?>
								<div class="col-sm-6 col-md-6 col-xs-12">
									<div class="row">
									<?php endif; ?>
									<div class="col-sm-3 col-6">
										<div class="sponsor-wrap">
											<a target="_blank" href="<?= get_sub_field('sponsor_link'); ?>">
												<img src="<?= get_sub_field('sponsor_image'); ?>" class="center-block img img-responsive" />
											</a>
										</div>
									</div>
									<?php if (($count + 1) == $maxSizeBronzes) : ?>
									</div>
								</div>
							<?php endif; ?>
							<?php ++$count; ?>
					<?php endwhile;
					else :
					endif; ?>
			<?php endwhile;
			else :
			endif; ?>

			<div class="col-md-12 text-center" style="font-family:Quicksand">
				Copyright © Genomics Medicine Ireland 2019
			</div>
			<!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script nonce='<?= NONCE_SEC ?>' type="text/javascript">
	_linkedin_partner_id = "1748937";
	window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
	window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script>
<script nonce='<?= NONCE_SEC ?>' type="text/javascript">
	(function() {
		var s = document.getElementsByTagName("script")[0];
		var b = document.createElement("script");
		b.type = "text/javascript";
		b.async = true;
		b.nonce = '<?= NONCE_SEC ?>';
		b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		s.parentNode.insertBefore(b, s);
	})();
</script>
<noscript>
	<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1748937&fmt=gif" />
</noscript>

</body>

</html>