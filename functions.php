<?php

/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ($understrap_includes as $file) {
	$filepath = locate_template('inc' . $file);
	if (!$filepath) {
		trigger_error(sprintf('Error locating /inc%s for inclusion', $file), E_USER_ERROR);
	}
	require_once $filepath;
}

remove_filter('template_redirect', 'redirect_canonical');

// block WP enum scans
// https://m0n.co/enum
if (!is_admin()) {
	// default URL format
	if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die();
	add_filter('redirect_canonical', 'shapeSpace_check_enum', 10, 2);
}
function shapeSpace_check_enum($redirect, $request)
{
	// permalink URL format
	if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die();
	else return $redirect;
}

function add_script_attributes($html, $handle)
{
	if ('understrap-scripts' === $handle) {
		return str_replace("></script>", " integrity='".generateSRI($html)."' crossorigin='anonymous'></script>", $html);
	}
	if ('gs2020' === $handle) {
		return str_replace("></script>", " integrity='".generateSRI($html)."' crossorigin='anonymous'></script>", $html);
	}
	if ('jquery-core' === $handle) {
		return str_replace("></script>", " integrity='".generateSRI($html)."' crossorigin='anonymous'></script>", $html);
	}
	if ('jquery-migrate' === $handle) {
		return str_replace("></script>", " integrity='".generateSRI($html)."' crossorigin='anonymous'></script>", $html);
	}
	if ('wp-embed' === $handle) {
		return str_replace("></script>", " integrity='".generateSRI($html)."' crossorigin='anonymous'></script>", $html);
	}
	return $html;
}
add_filter('script_loader_tag', 'add_script_attributes', 10, 2);

function generateSRI($filepath)
{
	$data = file_get_contents(getJSSRC($filepath));
	return 'sha256-'.base64_encode(hash("sha256", $data, true));
}

function getJSSRC($input_string)
{
	$count = preg_match('/src=(["\'])(.*?)\1/', $input_string, $match);
	if ($count){
		return ($match[2] . "\n");
	}
}

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

add_filter('rest_authentication_errors', 'wpse150207_filter_incoming_connections');

function wpse150207_filter_incoming_connections($errors)
{
	die;
}