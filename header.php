<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod('understrap_container_type');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!-- security -->
<?php
header("Content-Security-Policy:default-src 'self' 'unsafe-inline' fonts.googleapis.com snap.licdn.com *.genomicssummitdublin.com https://genomicssummitdublin.com https://www.genomicssummitdublin.com;style-src 'self' 'unsafe-inline' fonts.googleapis.com *.genomicssummitdublin.com https://genomicssummitdublin.com https://www.genomicssummitdublin.com; font-src 'self' fonts.gstatic.com;img-src 'self' px.ads.linkedin.com www.googletagmanager.com www.linkedin.com www.google-analytics.com *.genomicssummitdublin.com https://genomicssummitdublin.com https://www.genomicssummitdublin.com;script-src 'self' 'nonce-" . NONCE_SEC . "' *.genomicssummitdublin.com https://genomicssummitdublin.com https://www.genomicssummitdublin.com;");
?>

<head>
	<script nonce='<?= NONCE_SEC ?>' async src="https://www.googletagmanager.com/gtag/js?id=UA-154017912-1"></script>
	<script nonce='<?= NONCE_SEC ?>'>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-154017912-1');
	</script>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Quicksand" />
	<link rel="stylesheet" type="text/css" href="<?= TEMPLATE_PATH ?>/css/gs2020.css" />
	<style>
		@font-face {
			font-family: "Jaapokki Regular";
			src: url("<?= TEMPLATE_PATH ?>/fonts/jaapokki-regular.eot"),
				url("<?= TEMPLATE_PATH ?>/fonts/jaapokki-regular.woff"),
				url("<?= TEMPLATE_PATH ?>/fonts/jaapokki-regular.ttf");
		}
	</style>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action('wp_body_open'); ?>
	<div class="site" id="page">

		<!-- ******************* The Navbar Area ******************* -->
		<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

			<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e('Skip to content', 'understrap'); ?></a>

			<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-primary">

				<div class="container">

					<!-- Your site title as branding in the menu -->
					<?php //if ( ! has_custom_logo() ) { 
					?>

					<?php //if ( is_front_page() && is_home() ) : 
					?>

					<h1 class="navbar-brand mb-0"><a rel="home" href="<?php //echo esc_url( home_url( '/' ) ); 
																		?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); 
																					?>" itemprop="url"><?php //bloginfo( 'name' ); 
																										?></a></h1>

					<?php //else : 
					?>

					<a class="navbar-brand" rel="home" href="<?php //echo esc_url( home_url( '/' ) ); 
																?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); 
																			?>" itemprop="url"><?php //bloginfo( 'name' ); 
																								?></a>

					<?php //endif; 
					?>


					<?php //} else {

					//} 
					?>
					<!-- end custom logo -->
					<?php //the_custom_logo(); 
					?>
					<a href="/#home2020" rel="home" itemprop="url"><img src="/wp-content/uploads/2019/11/gs2020logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e('Toggle navigation', 'understrap'); ?>">
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'container_class' => 'collapse navbar-collapse',
							'container_id'    => 'navbarNavDropdown',
							'menu_class'      => 'navbar-nav ml-auto',
							'fallback_cb'     => '',
							'menu_id'         => 'main-menu',
							'depth'           => 2,
							'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>
				</div><!-- .container -->

			</nav><!-- .site-navigation -->

		</div><!-- #wrapper-navbar end -->