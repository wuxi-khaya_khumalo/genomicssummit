jQuery(function ($) {
    $('.speaker-extra').click(function () {

        $("#speaker-extra-title").html($(this).data("title"));
        $("#speaker-extra-image").attr("src", $(this).data("image"));
        $("#speaker-extra-blurb").html($(this).data("blurb"));
        $("#speaker-extra-bio").html($(this).data("bio"));

        $('#speaker-modal').modal('show');
    });

    $("#aviva-image").click(function () {
        $('#venue-modal').modal('show');
    });
});