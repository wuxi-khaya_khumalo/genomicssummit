<?php
/* Template Name: GS2020 */
get_header();
?>

<section class="aviva2020">
    <div id="home2020">
        <div class="row">
            <img src="<?= the_field("home_logo") ?>" class="home-logo-2020 img img-responsive aligncenter" />
        </div>

        <section class="row">
            <div class="container">
                <div class="col-md-6 offset-md-3">
                    <h4 class="gs2020-intro"><?= the_field("home_blurb") ?></h4>
                </div>
            </div>
        </section>

        <!-- <section class="row reg-2020">
            <div class="container">
                <div class="col-md-6 offset-md-3">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <img src="<?= the_field("cpd_stamp") ?>" class="img img-responsive aligncenter" />
                        </div>
                        <div class="col-md-6 col-xs-12 register-2020">
                            <?= the_field("register_now_text") ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="row text-center">
            <small class="co-host-text-2020 aligncenter"><?= the_field("co_host_title") ?></small>
        </section> -->

        <section class="row text-center">
            <img src="<?= the_field("co_hosts_image") ?>" class="co-hosts-image-2020 img img-responsive aligncenter" />
        </section>
    </div>

    <div id="venue2020">
        <!-- <div class="container">
            <h1 class="main-title-2020"><?= the_field("section_venue_title") ?></h1>
        </div> -->

        <section class="container pt-2">
            <!-- <div class="row">
                <div class="col-md-12 col-xs-12">
                    <h6 class="location-2020"><?= the_field("main_title") ?></h6>
                    <span class="agenda-blurbs-2020">
                        <p><a target="_blank" href="<?= the_field("aviva_link") ?>"><?= the_field("where_to_find_us") ?></a></p>
                    </span>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-12 location-blurb-2020 pt-5">
                    <p class="h1"><?= the_field("main_blurb") ?></p>
                </div>
                <!-- <div class="col-md-2 col-sm-2 col-xs-12">
                    <img style="background: rgba(255, 255, 255, .1);" id="aviva-image" src="<?= the_field("aviva_image") ?>" class="img img-responsive" />
                </div> -->
            </div>
        </section>
    </div>
</section>

<section class="crowd2020" id="agenda2020">
    <div class="container">
        <h1 class="main-title-2020"><?= the_field("section_agenda_title") ?></h1>
        <h6 class="gs2020-date"><?= the_field("summit_date") ?></h6>
    </div>

    <div class="container">
        <div class="row">
            <?php $count = 1; ?>
            <?php if (have_rows('agenda_items')) :
                while (have_rows('agenda_items')) : the_row();
                    $duration = get_sub_field('duration');
                    $blurb = get_sub_field('blurb');
            ?>

                    <?php if (($count == 1) || ((floor(count(get_field('agenda_items')) / 2) + 1) == $count)) {
                        echo '<div class="2020box col-md-6 col-xs-12">';
                    } ?>

                    <div class="agenda-wrap-2020">
                        <p class="agenda-blurbs-2020"><b><?= $duration ?></b></p>
                        <div class="agenda-blurbs-2020"><?= $blurb ?></div>
                    </div>

                    <?php if ((floor(count(get_field('agenda_items')) / 2) == $count) || ($count == count(get_field('agenda_items')))) {
                        echo '</div>';
                    } ?>
                    <?php ++$count; ?>
            <?php endwhile;
            else :
            endif; ?>
        </div>
    </div>
</section>

<section class="speakers2020" id="speakers2020">
    <div class="container">
        <h1 class="main-title-2020"><?= the_field("section_speaker_title") ?></h1>
    </div>

    <div class="container">
        <?php $count = 0; ?>
        <?php if (have_rows('speaker_items')) :
            echo "<div class='row'>";
            while (have_rows('speaker_items')) : the_row();
                $title = get_sub_field('speaker_title');
                $blurb = get_sub_field('speaker_blurb');
                $bio = htmlentities(get_sub_field("speaker_bio"));
                $image = get_sub_field("speaker_image");
        ?>
                <div class="col-md-3 col-sm-6 col-xs-12 speaker-extra" data-id="<?= get_row_index(); ?>" data-blurb="<?= $blurb ?>" data-title="<?= $title ?>" data-image="<?= $image ?>" data-bio="<?= $bio ?>">
                    <div class="container-speaker">
                        <img id="speaker-image-<?= get_row_index(); ?>" src="<?= $image ?>" class="img img-responsive zoom" />
                        <div class="speaker-wrap-2020">
                            <p class="speaker-title-2020"><b><?= $title ?></b></p>
                            <div class="speaker-blurbs-2020"><?= strlen($blurb) > 50 ? substr($blurb, 0, 55) . "..." : $blurb ?></div>
                        </div>
                    </div>
                </div>
                <?php ++$count; ?>
                <?php if (($count == 0) || ($count % 4 == 0)) {
                    echo '</div><div class="row">';
                } ?>
        <?php endwhile;
            echo "</div>";
        else :
        endif; ?>
    </div>
</section>

<div id="speaker-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <img src="" id="speaker-extra-image" class="img img-responsive" />
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <button type="button" class="close" data-dismiss="modal"><b>&times;</b></button>
                        <h1 id="speaker-extra-title"></h1>
                        <p id="speaker-extra-blurb"></p>
                        <p id="speaker-extra-bio"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="venue-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <a target="_blank" href="<?= the_field("aviva_link") ?>"><img src="<?= the_field("full_aviva_image") ?>" class="img img-responsive aligncenter" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>